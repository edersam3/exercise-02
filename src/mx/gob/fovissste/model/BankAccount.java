package mx.gob.fovissste.model;

import java.io.Serializable;

/**
 * @author Eder Sam
 *
 */
public class BankAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6150473347504761449L;

	private String number;

	private Float balance;

	public BankAccount(String number, Float balance) {
		this.number = number;
		this.balance = balance;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Float getBalance() {
		return balance;
	}

	public void setBalance(Float balance) {
		this.balance = balance;
	}

}
