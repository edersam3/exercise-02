package mx.gob.fovissste.model;

import java.io.Serializable;

/**
 * @author Eder Sam
 *
 */
public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8748810931928851L;

	private String name;

	private BankAccount account;

	public Client(String name, BankAccount account) {
		this.name = name;
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BankAccount getAccount() {
		return account;
	}

	public void setAccount(BankAccount account) {
		this.account = account;
	}

	public void getAccountData() {
		if (account != null) {
			System.out.println("Número de cuenta: " + account.getNumber());
			System.out.println("Saldo: " + account.getBalance());
		} else {
			System.out.println("No existe información para el cliente solicitado.");
		}
	}

}
