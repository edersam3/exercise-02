package mx.gob.fovissste;

import mx.gob.fovissste.model.BankAccount;
import mx.gob.fovissste.model.Client;

/**
 * @author Eder Sam
 *
 */
public class Main {

	public static void main(String[] args) {
		BankAccount account = new BankAccount("123456-7", 10500.75F);
		Client client = new Client("Juan Perez", account);
		client.getAccountData();
	}

}
