2. Tenemos una clase Cliente, la cual representa a todos los clientes de un banco. Se observa
que la clase Cliente va a tener un atributo de tipo CuentaBancaria. La clase CuentaBancaria
va a tener los siguientes atributos:

	a. numero, de tipo String y private.
	b. saldo, de tipo float y private.

Dentro de la clase Cliente, debe haber un atributo de tipo String llamado nombre y un atributo
cuenta de tipo CuentaBancaria. Al inicializar el atributo de instancia cuenta de tipo CuentaBancaria, debe ser de la siguiente manera, por ejemplo:

	cuenta = new CuentaBancaria(“123456-7”, 10500.75);

En la misma clase Cliente, debe haber un método de instancia llamado obtenerDatosDeCuenta()
que debe imprimir el número de la cuenta y el saldo que tiene hasta ese momento. El constructor
de la clase Cliente será de la siguiente manera:

	public Cliente(String nombre, CuentaBancaria cuenta)
	{
	 this.nombre = nombre;
	 this.cuenta = cuenta;
	}

De tal manera que en el método main() de la clase Cliente vamos a crear un objeto de tipo Cliente.
Al crear un objeto de tipo Cliente, previamente debe crearse un objeto de tipo cuenta (porque debe pasarse al constructor de la clase Cliente) para que se haga de la siguiente manera:

	cuenta = new CuentaBancaria(“123456-7”, 10500.75);

	Cliente cliente = new Cliente(“Juan Perez”, cuenta);
	
Y a partir de esta referencia llamada cliente, debe invocarse al método obtenerDatosDeCuenta()
para observar los datos propios del objeto cuenta.